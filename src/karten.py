from format_roman import format_roman

class Karte:
    def __init__(self, klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation):
        self.klasse = klasse
        self.stichworte = stichworte
        self.beschreibung = beschreibung
        self.hinweis = hinweis
        self.frage = frage
        self.anregung = anregung
        self.affirmation = affirmation

    def print(self):
        print("Fehler in der Matrix")

    def getString(self):
        string = "Fehler in der Matrix"
        return string
    
    def getName(self):
        pass 
    def getPicturePath(self):
       pass 

class ArcanumGross(Karte):
    def __init__(self, nummer, roemischesZeichen, klasse, name, stichworte, beschreibung, hinweis, frage, anregung, affirmation):
        super().__init__(klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation)
        self.nummer = nummer
        self.name = name
        self.roemischesZeichen = roemischesZeichen

    def print(self):
        print(f"\n{self.roemischesZeichen} {self.name}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n\n")

    def getString(self):
        string = f"\n{self.roemischesZeichen} {self.name}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n\n"
        return string

    def getName(self):
        name_ = f"{self.roemischesZeichen} {self.name}"
        return name_

    def getPicturePath(self):
        if self.nummer < 10:
            name_ = "0" + str(self.nummer) + self.name
        else:
            name_ = str(self.nummer) + self.name
        path = pfadzurkarte(name_) + name_.replace(" ", "") + ".jpg"
        return path



class ArcanumKlein(Karte):
    def __init__(self, klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation):
        super().__init__(klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation)
        


class Hofkarte(ArcanumKlein):
    def __init__(self, hierarchie, element, klasse,  stichworte, beschreibung, hinweis, frage, anregung, affirmation):
        super().__init__(klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation)
        self.hierarchie = hierarchie
        self.element = element

    def print(self):
        print(f"\n{self.hierarchie} der {self.element}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n")

    def getString(self):
        string = f"\n{self.hierarchie} der {self.element}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n"
        return string

    def getName(self):
        name = "{self.hierarchie} der {self.element}"
        return name

    def getPicturePath(self):
        name_ = self.hierarchie + "der " + self.element
        path = pfadzurkarte(name_) + name_.replace(" ", "") + ".jpg"
        return path


class Zahlenkarte(ArcanumKlein):
    def __init__(self, nummer, element, klasse, name, stichworte, beschreibung, hinweis, frage, anregung, affirmation):
        super().__init__(klasse, stichworte, beschreibung, hinweis, frage, anregung, affirmation)
        self.nummer = nummer
        self.element = element
        self.name = name

    def print(self):
        if self.nummer == "As":
            print(f"\n{self.nummer} der {self.element}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n")
        else:
            print(f"\n{self.nummer} {self.element}: {self.name}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n")
    
    def getString(self):
        if self.nummer == "As":
            string = f"\n{self.nummer} der {self.element}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n"
        else:
            string = f"\n{self.nummer} {self.element}: {self.name}\n\nStichworte: \n{self.stichworte}\n\nBeschreibung: \n{self.beschreibung}\n\nHinweis: \n{self.hinweis}\n\nFrage: \n{self.frage}\n\nAnregung: \n{self.anregung}\n\nAffirmation: \n{self.affirmation}\n"
        return string

    def namen(self):
        print(f"{self.nummer} {self.element} {self.name}")

    def getPicturePath(self):
        if self.nummer == "As":
            name_ = self.nummer + " der " + self.element
            path = pfadzurkarte(name_) + name_.replace(" ", "") + ".jpg"
        else:
            name_ = str(self.nummer) + " " + self.element + self.name
            path = pfadzurkarte(name_) + name_.replace(" ", "") + ".jpg"
        return path



# Initialisiere die Karten

allekarten = [
            "00 Der Narr", "01 Der Magier", "02 Die Hohepriesterin", "03 Die Kaiserin", "04 Der Kaiser", 
            "05 Der Hohepriester", "06 Die Liebenden", "07 Der Wagen", "08 Ausgleichung", "09 Der Eremit", 
            "10 Glueck", "11 Lust", "12 Der Gehängte", "13 Tod", "14 Kunst", "15 Der Teufel", "16 Der Turm", 
            "17 Der Stern", "18 Der Mond", "19 Die Sonne", "20 Das Aeon", "21 Das Universum", "Ritter der Stäbe", 
            "Königin der Stäbe", "Prinz der Stäbe", "Prinzessin der Stäbe", "Ritter der Kelche", 
            "Königin der Kelche", "Prinz der Kelche", "Prinzessin der Kelche", "Ritter der Schwerter", 
            "Königin der Schwerter", "Prinz der Schwerter", "Prinzessin der Schwerter", "Ritter der Scheiben", 
            "Königin der Scheiben", "Prinz der Scheiben", "Prinzessin der Scheiben", "As der Stäbe", 
            "2 Stäbe Herrschaft", "3 Stäbe Tugend", "4 Stäbe Vollendung", "5 Stäbe Streben", "6 Stäbe Sieg", 
            "7 Stäbe Tapferkeit", "8 Stäbe Schnelligkeit", "9 Stäbe Stärke", "10 Stäbe Unterdrückung", 
            "As der Kelche", "2 Kelche Liebe", "3 Kelche Fülle", "4 Kelche Üppigkeit", "5 Kelche Enttäuschung", 
            "6 Kelche Genuss", "7 Kelche Verderbnis", "8 Kelche Trägheit", "9 Kelche Freude", "10 Kelche Sattheit", 
            "As der Schwerter", "2 Schwerter Frieden", "3 Schwerter Kummer", "4 Schwerter Waffenruhe", 
            "5 Schwerter Niederlage", "6 Schwerter Wissenschaft", "7 Schwerter Vergeblichkeit", 
            "8 Schwerter Einmischung", "9 Schwerter Grausamkeit", "10 Schwerter Untergang", "As der Scheiben", 
            "2 Scheiben Wechsel", "3 Scheiben Arbeit", "4 Scheiben Macht", "5 Scheiben Quälerei", "6 Scheiben Erfolg", 
            "7 Scheiben Fehlschlag", "8 Scheiben Umsicht", "9 Scheiben Gewinn", "10 Scheiben Reichtum"
            ]


dasGrosseArcanum = allekarten[:22]
staebe_hof       = allekarten[22:26]
kelche_hof       = allekarten[26:30]
schwerter_hof    = allekarten[30:34]
scheiben_hof     = allekarten[34:38]
staebe_zahl      = allekarten[38:48]
kelche_zahl      = allekarten[48:58]
schwerter_zahl   = allekarten[58:68]
scheiben_zahl    = allekarten[68:78]
staebe           = staebe_hof + staebe_zahl
kelche           = kelche_hof + kelche_zahl
schwerter        = schwerter_hof + schwerter_zahl
scheiben         = scheiben_hof + scheiben_zahl
hofkarten        = staebe_hof + kelche_hof + schwerter_hof + scheiben_hof
zahlenkarten     = staebe_zahl + kelche_zahl + schwerter_zahl + scheiben_zahl
dasKleineArcanum = staebe + kelche + schwerter + scheiben


dictOfToken = {
            "Nummer":["<Nummer>", "</Nummer>"], 
            "Klasse":["<Klasse>", "</Klasse>"], 
            "Name":["<Name>", "</Name>"], 
            "Stichworte":["<Stichworte>", "</Stichworte>"], 
            "Beschreibung":["<Beschreibung>", "</Beschreibung>"], 
            "Hinweis":["<Hinweis>", "</Hinweis>"], 
            "Frage":["<Frage>", "</Frage>"], 
            "Anregung":["<Anregung>", "</Anregung>"], 
            "Affirmation":["<Affirmation>", "</Affirmation>"]
            }


def betweentoken(string, token1, token2):
    string=string.split(token1)[1].split(token2)[0]
    return string

attributeOfCards = {}

def readle(filename):
    with open(filename, "r") as reader:
      for i in reader.readlines():
        for s in dictOfToken:
          if dictOfToken.get(s)[0] in i:
            var=betweentoken(i, dictOfToken.get(s)[0], dictOfToken.get(s)[1])
            attributeOfCards[s]=var
      return attributeOfCards
    
########################################################################

def pfadzurkarte(karte):
  if karte in allekarten[0:22]:
    return "karten/DasGrosseArkanum/"
  if karte in allekarten[22:26]:
    return "karten/Hofkarten/Stäbe/"
  if karte in allekarten[26:30]:
    return "karten/Hofkarten/Kelche/"
  if karte in allekarten[30:34]:
    return "karten/Hofkarten/Schwerter/"
  if karte in allekarten[34:38]:
    return "karten/Hofkarten/Scheiben/"
  if karte in allekarten[38:48]:
    return "karten/DasKleineArkanum/Stäbe/"
  if karte in allekarten[48:58]:
    return "karten/DasKleineArkanum/Kelche/"
  if karte in allekarten[58:68]:
    return "karten/DasKleineArkanum/Schwerter/"
  if karte in allekarten[68:78]:
    return "karten/DasKleineArkanum/Scheiben/"



###############################################################
#
# Alle Karten werden in ein Dictionary karten_dict mit 
# dem Schlüssel in der Form "TKZahl" und dem Wert mit 
# dem Namen geschrieben
#
# Bsp.: TK01 = Der Magier
#       TK78 = Scheiben 10 Reichtum
#
###############################################################

karten_dict={}
karten_liste = []

# Initialisiere das große Arcanum
def correct_index(index, name):
    if index < 10:
        karten_liste.append("TK"+str(0)+str(index))
    else:
        karten_liste.append("TK"+str(index))
    karten_dict[karten_liste[index]]=name[2:]
    return karten_dict, karten_liste

for index, name in enumerate(dasGrosseArcanum):
    correct_index(index, name)
    attributeOfCards = readle(pfadzurkarte(name) + name.replace(" ", "") + ".xml")
    if index != 0:
        roman = format_roman("I", index)
    else:
        roman = 0

    karten_liste[index] = ArcanumGross(index, roman, "Das große Arcanum", karten_dict[karten_liste[index]], attributeOfCards["Stichworte"], attributeOfCards["Beschreibung"], attributeOfCards["Hinweis"], attributeOfCards["Frage"], attributeOfCards["Anregung"], attributeOfCards["Affirmation"])


# initialisiere die Hofkarten

def find_hierarchie(name):
    if name.find("der") != -1:
        found=name.find("der")
        hierarchie = name[:found]
    else:
        hierarchie=""
        print("Something happend here #find_hierarchie!")
    return hierarchie

def find_element(name):
    if name.find("Schwerter") != -1:
        element = "Schwerter"
    elif name.find("Scheiben") != -1:
        element = "Scheiben"
    elif name.find("Kelche") != -1:
        element = "Kelche"
    elif name.find("Stäbe") != -1:
        element = "Stäbe"
    else:
        element = ""
        print("Something happend here #find_element")
    return element

for index, name in enumerate(hofkarten, start=22):
        correct_index(index, name)
        attributeOfCards = readle(pfadzurkarte(name) + name.replace(" ", "") + ".xml")
        hierarchie = find_hierarchie(name)
        element = find_element(name)
        karten_liste[index] = Hofkarte(hierarchie, element, "Hofkarte", attributeOfCards["Stichworte"], attributeOfCards["Beschreibung"], attributeOfCards["Hinweis"], attributeOfCards["Frage"], attributeOfCards["Anregung"], attributeOfCards["Affirmation"])
     

# initialisiere die Zahlenkarten
def find_nummer(name):
    if name.find("As") != -1:
        nummer = "As"
        return nummer
    if name[1] == "0":
        nummer = "10"
        return nummer
    else:
        nummer = name[0]
        return nummer

for index, name in enumerate(zahlenkarten, start=38):
        correct_index(index, name)
        attributeOfCards = readle(pfadzurkarte(name) + name.replace(" ", "") + ".xml")
        element = find_element(name)
        nummer = find_nummer(name)
        if nummer == "As":
            name = ""
            karten_liste[index] = Zahlenkarte(nummer, element, "Zahlenkarte", name, attributeOfCards["Stichworte"], attributeOfCards["Beschreibung"], attributeOfCards["Hinweis"], attributeOfCards["Frage"], attributeOfCards["Anregung"], attributeOfCards["Affirmation"])
        else:
            name = name.replace(nummer + " " + element, "")
            karten_liste[index] = Zahlenkarte(nummer, element, "Zahlenkarte", name, attributeOfCards["Stichworte"], attributeOfCards["Beschreibung"], attributeOfCards["Hinweis"], attributeOfCards["Frage"], attributeOfCards["Anregung"], attributeOfCards["Affirmation"])

