import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from karten import *
from format_roman import format_roman
from tarot_cli import *

class mainWindow(QMainWindow):
    def __init__(self, parent = None):
        super(mainWindow, self).__init__(parent)
       
        # Main Layout
        self.mainLayout = QGridLayout()
        main_frame = QWidget()
        self.text = QLabel()
        self.image = QLabel()

        # Trigger Actions -> Menu Bar
        self.quitTheProgram = self.quit

        # Menu Bar
        bar = self.menuBar()
        menu = bar.addMenu("Optionen")
        
        beenden = QAction("Beenden", self, triggered=self.quitTheProgram)
        menu.addAction(beenden)

        # Buttons
        button_zufaelligeKarte = QPushButton("Zufällige Karte ziehen")
        button_bestimmteKarte = QPushButton("Bestimmte Karte ziehen")
        

        # Trigger Actions -> Buttons
        button_zufaelligeKarte.clicked.connect(self.zufaelligeKarte)
        button_bestimmteKarte.clicked.connect(self.bestimmteKarte)

        #  Layout

        self.mainLayout.addWidget(button_zufaelligeKarte, 1, 0)
        self.mainLayout.addWidget(button_bestimmteKarte, 1, 1)

        self.mainLayout.addWidget(self.text, 2, 0)
        self.mainLayout.addWidget(self.image, 2, 1)
        
        main_frame.setLayout(self.mainLayout)
        self.setCentralWidget(main_frame)
        self.setWindowTitle("Toth Tarot")

    def quit(self):
        print("quit")
        self.close()

    def zufaelligeKarte(self):
        karte = karten_liste[random.randint(0,77)]
        self.text.setText(karte.getString())
        self.text.setWordWrap(True)
        pixmap = QPixmap(karte.getPicturePath())
        self.image.setPixmap(pixmap)

    def bestimmteKarte(self):
        self.text.setText("Zu welcher Karte möchtest du die Beschreibung lesen?")
        items = allekarten
        item, ok = QInputDialog.getItem(self, "Suche die Karte", 
                "Karten", items, 0, False)
        if ok and item:
            self.text.setText(item)
            index = allekarten.index(item)
        self.text.setText(karten_liste[index].getString())
        pixmap = QPixmap(karten_liste[index].getPicturePath())
        self.image.setPixmap(pixmap)
        self.text.setWordWrap(True)

def main():
    app = QApplication(sys.argv)
    ex2 = mainWindow()
    ex2.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
