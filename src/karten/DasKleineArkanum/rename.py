import os
import sys


allekarten = [
            "00 Der Narr", "01 Der Magier", "02 Die Hohepriesterin", "03 Die Kaiserin", "04 Der Kaiser", 
            "05 Der Hohepriester", "06 Die Liebenden", "07 Der Wagen", "08 Ausgleichung", "09 Der Eremit", 
            "10 Glueck", "11 Lust", "12 Der Gehängte", "13 Tod", "14 Kunst", "15 Der Teufel", "16 Der Turm", 
            "17 Der Stern", "18 Der Mond", "19 Die Sonne", "20 Das Aeon", "21 Das Universum", "Ritter der Stäbe", 
            "Königin der Stäbe", "Prinz der Stäbe", "Prinzessin der Stäbe", "Ritter der Kelche", 
            "Königin der Kelche", "Prinz der Kelche", "Prinzessin der Kelche", "Ritter der Schwerter", 
            "Königin der Schwerter", "Prinz der Schwerter", "Prinzessin der Schwerter", "Ritter der Scheiben", 
            "Königin der Scheiben", "Prinz der Scheiben", "Prinzessin der Scheiben", "As der Stäbe", 
            "2 Stäbe Herrschaft", "3 Stäbe Tugend", "4 Stäbe Vollendung", "5 Stäbe Streben", "6 Stäbe Sieg", 
            "7 Stäbe Tapferkeit", "8 Stäbe Schnelligkeit", "9 Stäbe Stärke", "10 Stäbe Unterdrückung", 
            "As der Kelche", "2 Kelche Liebe", "3 Kelche Fülle", "4 Kelche Üppigkeit", "5 Kelche Enttäuschung", 
            "6 Kelche Genuss", "7 Kelche Verderbnis", "8 Kelche Trägheit", "9 Kelche Freude", "10 Kelche Sattheit", 
            "As der Schwerter", "2 Schwerter Frieden", "3 Schwerter Kummer", "4 Schwerter Waffenruhe", 
            "5 Schwerter Niederlage", "6 Schwerter Wissenschaft", "7 Schwerter Vergeblichkeit", 
            "8 Schwerter Einmischung", "9 Schwerter Grausamkeit", "10 Schwerter Untergang", "As der Scheiben", 
            "2 Scheiben Wechsel", "3 Scheiben Arbeit", "4 Scheiben Macht", "5 Scheiben Quälerei", "6 Scheiben Erfolg", 
            "7 Scheiben Fehlschlag", "8 Scheiben Umsicht", "9 Scheiben Gewinn", "10 Scheiben Reichtum"
            ]



staebe_zahl      = allekarten[39:48]
staebe_zahl.sort()
kelche_zahl      = allekarten[49:58]
kelche_zahl.sort()
schwerter_zahl   = allekarten[59:68]
schwerter_zahl.sort()
scheiben_zahl    = allekarten[69:78]
scheiben_zahl.sort()


##########
#Scheiben
##########

scheiben_list = []
for root, dirs, files in os.walk("Scheiben"):
    for i in files:
        scheiben_list.append(i)

scheiben_list.sort()
scheiben_list.pop()
scheiben_list.pop()

for i in scheiben_list:
    append=i[-4:]
    if i[1] == "0":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[0].replace(" ", "")+append)
    if i[0] == "2":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[1].replace(" ", "")+append)
    if i[0] == "3":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[2].replace(" ", "")+append)
    if i[0] == "4":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[3].replace(" ", "")+append)
    if i[0] == "5":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[4].replace(" ", "")+append)
    if i[0] == "6":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[5].replace(" ", "")+append)
    if i[0] == "7":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[6].replace(" ", "")+append)
    if i[0] == "8":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[7].replace(" ", "")+append)
    if i[0] == "9":
        os.rename("./Scheiben/"+i, "./Scheiben/"+scheiben_zahl[8].replace(" ", "")+append)

########
#Kelche
########

kelche_list = []
for root, dirs, files in os.walk("Kelche"):
    for i in files:
        kelche_list.append(i)

kelche_list.sort()
kelche_list.pop()
kelche_list.pop()

for i in kelche_list:
    append=i[-4:]
    if i[1] == "0":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[0].replace(" ", "")+append)
    if i[0] == "2":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[1].replace(" ", "")+append)
    if i[0] == "3":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[2].replace(" ", "")+append)
    if i[0] == "4":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[3].replace(" ", "")+append)
    if i[0] == "5":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[4].replace(" ", "")+append)
    if i[0] == "6":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[5].replace(" ", "")+append)
    if i[0] == "7":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[6].replace(" ", "")+append)
    if i[0] == "8":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[7].replace(" ", "")+append)
    if i[0] == "9":
        os.rename("./Kelche/"+i, "./Kelche/"+kelche_zahl[8].replace(" ", "")+append)

###########
#Schwerter
###########

schwerter_list = []
for root, dirs, files in os.walk("Schwerter"):
    for i in files:
        schwerter_list.append(i)

schwerter_list.sort()
schwerter_list.pop()
schwerter_list.pop()

for i in schwerter_list:
    append=i[-4:]
    if i[1] == "0":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[0].replace(" ", "")+append)
    if i[0] == "2":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[1].replace(" ", "")+append)
    if i[0] == "3":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[2].replace(" ", "")+append)
    if i[0] == "4":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[3].replace(" ", "")+append)
    if i[0] == "5":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[4].replace(" ", "")+append)
    if i[0] == "6":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[5].replace(" ", "")+append)
    if i[0] == "7":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[6].replace(" ", "")+append)
    if i[0] == "8":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[7].replace(" ", "")+append)
    if i[0] == "9":
        os.rename("./Schwerter/"+i, "./Schwerter/"+schwerter_zahl[8].replace(" ", "")+append)

#######
#Stäbe
#######

staebe_list = []
for root, dirs, files in os.walk("Stäbe"):
    for i in files:
        staebe_list.append(i)

staebe_list.sort()
staebe_list.pop()
staebe_list.pop()

for i in staebe_list:
    append=i[-4:]
    if i[1] == "0":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[0].replace(" ", "")+append)
    if i[0] == "2":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[1].replace(" ", "")+append)
    if i[0] == "3":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[2].replace(" ", "")+append)
    if i[0] == "4":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[3].replace(" ", "")+append)
    if i[0] == "5":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[4].replace(" ", "")+append)
    if i[0] == "6":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[5].replace(" ", "")+append)
    if i[0] == "7":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[6].replace(" ", "")+append)
    if i[0] == "8":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[7].replace(" ", "")+append)
    if i[0] == "9":
        os.rename("./Stäbe/"+i, "./Stäbe/"+staebe_zahl[8].replace(" ", "")+append)


