#!/usr/bin/python

import sys
#sys.path.insert(0, '/home/martrix/Programmieren/Python/Tarot/TheGameOfTarot/modules')
#sys.path.insert(0, './module')
#sys.path.insert(0, './karten')
from initialisiereKarten import *
from karten import *
from format_roman import format_roman
import random
import fnmatch
import os
import re
from graphics import *

#------------------------------------------------------------------------------#
# Programname: tarot.py                                                        #
#                                                                              #
# Autor: Martrix                                                               #
#                                                                              #
# Ein Programm bei dem Wahlweise Karten gezogen werden können (mit verschieden #
# en Legemustern) und die Bedeutung sowohl der Legemuster als auch der Karten  #
# angezeigt werden kann. Weitere Modifikationen möglich                        #
#------------------------------------------------------------------------------#



def menu():
    print("Was möchtest du tun?")
    print("1 - eine zufällige Karte ziehen")
    print("2 - die Beschreibung einer bestimmten Karte lesen")
    print("q - Programm beenden")
    print()
    wahl = input()
    if wahl == "1":
        karten_liste[random.randint(0,77)].print()
    elif wahl == "2":
        for i in range(0,50):
            print()
        print("Zu welcher Karte möchtest du die Beschreibung lesen?")
        for index, karte in enumerate(karten_liste, start=0):
            print(index, end = " - ")
            karten_liste[index].namen()
        print()
        print("Bitte die entsprechende Nummer eingeben: ")
        welchekarte = int(input())
        karten_liste[welchekarte].print()
    elif wahl == "q":
        progress = 0
        return progress
    else:
        for i in range(0,20):
            print()
        print("Falsche Eingabe...")
        print()
        print()

def main():
    app = QApplication(sys.argv)
    window = mainWindow()
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
