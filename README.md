# Toth Tarot

Ein Tarot Spiel geschrieben in Python

Starten des Spiels:
```python tarot_cli.py```

## Ordner

+---tothtarot
|   +---README.md
|   +---src
|   |   +---tarot_cli.py
|   |   +---graphics.py
|   |   +---format_roman.py
|   |   +---karten.py
|   |   +---karten
|   |   |   +---DasGrosseArcanum
|   |   |   |   ...xml
|   |   |   |   ...jpg
|   |   |   +---DasKleineArcanum
|   |   |   |   +---Schwerter
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Kelche
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Scheiben
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Stäbe
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   +---Hofkarten
|   |   |   |   +---Schwerter
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Kelche
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Scheiben
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg
|   |   |   |   +---Stäbe
|   |   |   |   |   ...xml
|   |   |   |   |   ...jpg




### karten
Alle Karten sind hier in xml und jpg vorhanden 

### module


